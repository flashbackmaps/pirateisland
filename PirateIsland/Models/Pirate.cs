﻿using System.Collections.Generic;

namespace PirateIsland
{
    public class Pirate : Tile
    {
        public Pirate(int maxWidth, int maxHeight)
        {
            coorY = (maxHeight + 1) / 2;
            coorX = (maxWidth + 1) / 2;
        }

        public bool isInShip = false;
        public bool hasCoin = false;
        public string status = "Find the coin and get it to the ship!";
        public string symbol = "P";
        public void Move(int[] action, int maxWidth, int maxHeight, Dictionary<int, string> island)
        {
            string status;

            bool inIslandBorders = coorX + action[0] > 0 && coorX + action[0] < (maxWidth + 1) &&
                                   coorY + action[1] > 0 && coorY + action[1] < (maxHeight + 1);
            bool isGoingToShip = (coorY + action[1]) == maxHeight + 1 &&
                                 island[maxHeight + 1][((coorX + action[0]) * 2) - 1] == 'S';
            if (inIslandBorders || isGoingToShip)
            {
                coorX += action[0];
                coorY += action[1];
                isInShip = isGoingToShip;
            }

            if (island[coorY][coorX * 2 - 1] == '$')
            {
                status = "You`ve got the coin - go to the ship!";
                island[coorY] = island[coorY].Remove(coorX * 2 - 1, 1).Insert(coorX * 2 - 1, "_");
                hasCoin = true;
            }
        }
    }
}
