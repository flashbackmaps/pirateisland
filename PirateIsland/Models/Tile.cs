﻿namespace PirateIsland
{
    public class Tile
    {
        public int coorX { get; set; }
        public int coorY { get; set; }
        public string symbol { get; set; }
    }
}
