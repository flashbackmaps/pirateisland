﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace PirateIsland
{
    public static class IslandUtils
    {
        private static string line = "|_|_|_|_|_|_|_|";
        public static Dictionary<int, string> InitIsland(int maxWidth, int maxHeight, string[] arrows)
        {
            Random rand = new Random();

            Dictionary<int, string> island = new Dictionary<int, string>();

            List<Tile> arrowTiles = new List<Tile>();
            Tile coin = new Tile()
            {
                coorX = rand.Next(1, maxWidth),
                coorY = rand.Next(1, maxHeight),
                symbol = "$"
            };

            for (int i = 1; i < 11; i++)
            {
                int x = rand.Next(1, maxWidth);
                int y = rand.Next(1, maxHeight);
                int randomArrow = rand.Next(0, 3);
                arrowTiles.Add(new Tile()
                {
                    coorX = x,
                    coorY = y,
                    symbol = arrows[randomArrow]
                });
            }

            for (int i = 1; i < maxHeight + 1; i++)
            {
                string row = line;
                foreach (Tile tile in arrowTiles.Where(x => x.coorY == i))
                {
                    row = row.Remove(tile.coorX * 2 - 1, 1).Insert(tile.coorX * 2 - 1, tile.symbol);
                }

                if (coin.coorY == i)
                    row = row.Remove(coin.coorX * 2 - 1, 1).Insert(coin.coorX * 2 - 1, coin.symbol);

                island.Add(i, row);
            }
            island.Add(maxHeight + 1, "       S       ");

            return island;
        }
    }
}
