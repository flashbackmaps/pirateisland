﻿using System;
using System.Collections.Generic;

namespace PirateIsland
{
    class Program
    {
        private static int maxWidth = 7, maxHeight = 7;
        private static string[] arrows = new[] { "↓", "↑", "→", "←"};

        private static Dictionary<ConsoleKey, int[]> actions = new Dictionary<ConsoleKey, int[]>()
        {
            {ConsoleKey.NumPad8, new int[2] {0, -1} },
            {ConsoleKey.NumPad9, new int[2] {1, -1} },
            {ConsoleKey.NumPad6, new int[2] {1, 0} },
            {ConsoleKey.NumPad3, new int[2] {1, 1} },
            {ConsoleKey.NumPad2, new int[2] {0, 1} },
            {ConsoleKey.NumPad1, new int[2] {-1, 1} },
            {ConsoleKey.NumPad4, new int[2] {-1, 0} },
            {ConsoleKey.NumPad7, new int[2] {-1, -1} }
        };


        private static Dictionary<string, int[]> arrowActions = new Dictionary<string, int[]>()
        {
            {"↓", new int[2] {0, 1} },
            {"↑", new int[2] {0, -1} },
            {"→", new int[2] {1, 0} },
            {"←", new int[2] {-1, 0} }
        };

        static void Main(string[] args)
        {
            //prepare console
            Console.OutputEncoding = System.Text.Encoding.UTF8;
            Console.Clear();

            var island = IslandUtils.InitIsland(maxWidth, maxHeight, arrows);  //Creating random island with coin and arrows

            //prepare pirate
            Pirate pirate = new Pirate(maxWidth, maxHeight);

            ConsoleKeyInfo keyInfo = new ConsoleKeyInfo();
            while (!(keyInfo.Key == ConsoleKey.Escape || pirate.isInShip))  //the game cycle
            {
                while (arrowActions.ContainsKey(island[pirate.coorY].Substring(pirate.coorX * 2 - 1, 1)))  //check arrows
                {
                    pirate.Move(
                        new[]
                        {
                            arrowActions[island[pirate.coorY].Substring(pirate.coorX * 2 - 1, 1)][0],
                            arrowActions[island[pirate.coorY].Substring(pirate.coorX * 2 - 1, 1)][1]
                        }, maxWidth, maxHeight, island);
                }

                Console.Clear();

                Console.WriteLine(pirate.status);
                Console.WriteLine();

                for (int i = 1; i < maxHeight + 1; i++)  //redraw island
                {
                    if (pirate.coorY == i)
                    {
                        Console.WriteLine(island[i].Remove(pirate.coorX * 2 - 1, 1).Insert(pirate.coorX * 2 - 1, pirate.symbol));
                    }
                    else
                    {
                        Console.WriteLine(island[i]);
                    }
                }

                Console.WriteLine(island[maxHeight + 1]); //draw the ship

                keyInfo = Console.ReadKey(true);

                if (actions.ContainsKey(keyInfo.Key)) //do action for the pirate
                {
                    int[] action = actions[keyInfo.Key];
                    
                    pirate.Move(action, maxWidth, maxHeight, island);
                }
            }

            if (pirate.hasCoin)
            {
                Console.Clear();
                Console.WriteLine("You won! The coin is at the ship!");
                Console.ReadLine();
            }
            else
            {
                Console.Clear();
                Console.WriteLine("You lose! You left the coin at island!");
                Console.ReadLine();
            }
        }
    }
}
